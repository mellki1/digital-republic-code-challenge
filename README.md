# Paint Room

Este projeto tem o objetivo de ajudar calcular a quantidade de tinta necessária para pintar uma sala.

# API
Esta aplicação foi desenvolvida utilizando java e o framework SpringBoot.
A documentação da API está disponibilizada no: https://paint-room.herokuapp.com/swagger-ui.html#,
onde é possível testar e executar a aplicação.