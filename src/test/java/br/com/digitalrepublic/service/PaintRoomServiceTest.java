package br.com.digitalrepublic.service;

import br.com.digitalrepublic.dto.RequestDTO;
import br.com.digitalrepublic.dto.ResponseDTO;
import br.com.digitalrepublic.model.Wall;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class PaintRoomServiceTest {

    @Autowired
    private PaintRoomService paintRoomService;

    private List<Wall> getWallListWithValidSizes() {
        List<Wall> wallList = new ArrayList<>();
        wallList.add(new Wall(3d, 5d));
        wallList.add(new Wall(2d, 5d));
        wallList.add(new Wall(2d, 5d));
        wallList.add(new Wall(3d, 5d));
        return wallList;
    }

    private List<Wall> getWallListWithInvalidSizes() {
        List<Wall> wallList = new ArrayList<>();
        wallList.add(new Wall(1d, 5d));
        wallList.add(new Wall(0d, 5d));
        wallList.add(new Wall(2d, 5d));
        wallList.add(new Wall(3d, 5d));
        return wallList;
    }

    @Test
    public void amountOfLitersNeeded_ok() {
        List<Wall> wallList = getWallListWithValidSizes();
        Double amountOfLitersNeeded = paintRoomService.amountOfLitersNeeded(wallList);
        assertThat(amountOfLitersNeeded).isEqualTo(10d);
    }

    @Test
    public void paintCansNeeded_ok() {
        List<Wall> wallList = getWallListWithValidSizes();
        LinkedHashMap<Double, Integer> paintCansNeeded = paintRoomService.paintCansNeeded(wallList);
        LinkedHashMap<Double, Integer> cansQuantityValid = new LinkedHashMap<>();
        cansQuantityValid.put(18d, 0);
        cansQuantityValid.put(3.6, 2);
        cansQuantityValid.put(2.5, 1);
        cansQuantityValid.put(0.5, 1);
        assertThat(paintCansNeeded).isEqualTo(cansQuantityValid);
    }

    @Test
    public void calculate_ok() {
        List<Wall> wallList = getWallListWithValidSizes();
        RequestDTO requestDTO = new RequestDTO(wallList);
        ResponseDTO responseDTO = paintRoomService.calculate(requestDTO);
        assertThat(responseDTO.getResponseMessage()).isEqualTo("Você precisará de 2 latas de 3,6L + 1 lata de 2,5L + 1 lata de 0,5L");
    }

    @Test
    public void calculate_InvalidInput_ok() {
        List<Wall> wallList = getWallListWithInvalidSizes();
        RequestDTO requestDTO = new RequestDTO(wallList);
        ResponseDTO responseDTO = paintRoomService.calculate(requestDTO);
        assertThat(responseDTO.getErrorMessage()).isEqualTo("A entrada está inválida. Fique atento(a) para as seguintes regras:\n" +
                "1 - Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados, mas podem possuir alturas e larguras diferentes;\n" +
                "2 - O total de área das portas e janelas deve ser no máximo 50% da área de parede;\n" +
                "3 - A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta;\n" +
                "4 - Cada janela possui as medidas: 2,00 x 1,20 mtos;\n" +
                "5 - Cada porta possui as medidas: 0,80 x 1,90;\n" +
                "6 - É necessário que a lista de paredes tenha 4 paredes com informações válidas;\n");
    }
}
