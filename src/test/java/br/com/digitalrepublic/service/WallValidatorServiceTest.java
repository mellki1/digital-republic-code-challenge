package br.com.digitalrepublic.service;

import br.com.digitalrepublic.model.Wall;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
public class WallValidatorServiceTest {

    @Autowired
    private WallValidatorService wallValidatorService;

    private Wall getWallWithValidSizes() {
        return new Wall(3d, 5d);
    }

    private Wall getWallWithSmallSizes() {
        return new Wall(0.224, 1d);
    }

    private Wall getWallWithBigSizes() {
        return new Wall(4d, 5d);
    }

    @Test
    public void wallSizeValidator_ValidInput_ok() {
        Wall wall = getWallWithValidSizes();
        Boolean wallSizeIsValid = wallValidatorService.wallSizeValidator(wall.getArea());
        assertThat(wallSizeIsValid).isNotNull();
        assertThat(wallSizeIsValid).isTrue();
        assertThat(wall.getArea()).isEqualTo(15d);
    }

    @Test
    public void wallSizeValidator_InputWithSmallSize_ok() {
        Wall wall = getWallWithSmallSizes();
        Boolean wallSizeIsValid = wallValidatorService.wallSizeValidator(wall.getArea());
        assertThat(wallSizeIsValid).isNotNull();
        assertThat(wallSizeIsValid).isFalse();
        assertThat(wall.getArea()).isEqualTo(0.224d);
    }

    @Test
    public void wallSizeValidator_InputWithBigSize_ok() {
        Wall wall = getWallWithBigSizes();
        Boolean wallSizeIsValid = wallValidatorService.wallSizeValidator(wall.getArea());
        assertThat(wallSizeIsValid).isNotNull();
        assertThat(wallSizeIsValid).isFalse();
        assertThat(wall.getArea()).isEqualTo(20d);
    }

    @Test
    public void wallSizeValidator_InputNull_ok() {
        Wall wall = new Wall(null, null);
        Boolean wallSizeIsValid = wallValidatorService.wallSizeValidator(wall.getArea());
        assertThat(wallSizeIsValid).isNotNull();
        assertThat(wallSizeIsValid).isFalse();
        assertThat(wall.getArea()).isEqualTo(0d);
    }

    @Test
    public void windowsAndDoorsQuantityValidator_ValidInput_ok() {
        Wall wall = getWallWithValidSizes();
        wall.setWindowsQuantity(2);
        wall.setDoorsQuantity(1);
        Boolean percentageWithWindowsAndDoorsIsValid = wallValidatorService.windowsAndDoorsQuantityValidator(wall);
        assertThat(percentageWithWindowsAndDoorsIsValid).isNotNull();
        assertThat(percentageWithWindowsAndDoorsIsValid).isTrue();
    }

    @Test
    public void windowsAndDoorsQuantityValidator_InvalidInput_ok() {
        Wall wall = getWallWithValidSizes();
        wall.setWindowsQuantity(7);
        wall.setDoorsQuantity(1);
        Boolean percentageWithWindowsAndDoorsIsValid = wallValidatorService.windowsAndDoorsQuantityValidator(wall);
        assertThat(percentageWithWindowsAndDoorsIsValid).isNotNull();
        assertThat(percentageWithWindowsAndDoorsIsValid).isFalse();
    }

    @Test
    public void windowsAndDoorsQuantityValidator_WithOutWindow_ok() {
        Wall wall = getWallWithValidSizes();
        wall.setDoorsQuantity(1);
        Boolean percentageWithWindowsAndDoorsIsValid = wallValidatorService.windowsAndDoorsQuantityValidator(wall);
        assertThat(percentageWithWindowsAndDoorsIsValid).isNotNull();
        assertThat(percentageWithWindowsAndDoorsIsValid).isTrue();
    }

    @Test
    public void windowsAndDoorsQuantityValidator_WithOutDoor_ok() {
        Wall wall = getWallWithValidSizes();
        wall.setWindowsQuantity(2);
        Boolean percentageWithWindowsAndDoorsIsValid = wallValidatorService.windowsAndDoorsQuantityValidator(wall);
        assertThat(percentageWithWindowsAndDoorsIsValid).isNotNull();
        assertThat(percentageWithWindowsAndDoorsIsValid).isTrue();
    }

    @Test
    public void windowsAndDoorsQuantityValidator_WithOutWindowAndDoor_ok() {
        Wall wall = getWallWithValidSizes();
        Boolean percentageWithWindowsAndDoorsIsValid = wallValidatorService.windowsAndDoorsQuantityValidator(wall);
        assertThat(percentageWithWindowsAndDoorsIsValid).isNotNull();
        assertThat(percentageWithWindowsAndDoorsIsValid).isTrue();
    }

    @Test
    public void inputValidator_ValidInput_ok() {
        Wall wall = getWallWithValidSizes();
        Boolean percentageWithWindowsAndDoorsIsValid = wallValidatorService.inputValidator(wall);
        assertThat(percentageWithWindowsAndDoorsIsValid).isNotNull();
        assertThat(percentageWithWindowsAndDoorsIsValid).isTrue();
    }

    @Test
    public void inputValidator_InvalidInput_ok() {
        Wall wall = getWallWithBigSizes();
        Boolean percentageWithWindowsAndDoorsIsValid = wallValidatorService.inputValidator(wall);
        assertThat(percentageWithWindowsAndDoorsIsValid).isNotNull();
        assertThat(percentageWithWindowsAndDoorsIsValid).isFalse();
    }
}
