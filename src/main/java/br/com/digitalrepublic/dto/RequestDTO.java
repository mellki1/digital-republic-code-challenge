package br.com.digitalrepublic.dto;

import br.com.digitalrepublic.model.Wall;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RequestDTO {
    private List<Wall> wall;
}
