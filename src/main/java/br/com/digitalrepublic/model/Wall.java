package br.com.digitalrepublic.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Wall {
    private Double width = 0d;
    private Double height = 0d;
    private Integer windowsQuantity = 0;
    private Integer doorsQuantity = 0;
    @JsonIgnore
    private Double area;

    public Wall(Double width, Double height) {
        this.width = width;
        this.height = height;
    }

    public Double getArea() {
        Double wallWidth = this.getWidth() != null ? this.getWidth() : 0d;
        Double wallHeight = this.getHeight() != null ? this.getHeight() : 0d;
        return wallWidth * wallHeight;
    }
}
