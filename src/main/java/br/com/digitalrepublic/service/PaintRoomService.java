package br.com.digitalrepublic.service;

import br.com.digitalrepublic.dto.RequestDTO;
import br.com.digitalrepublic.dto.ResponseDTO;
import br.com.digitalrepublic.model.Wall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class PaintRoomService {

    @Autowired
    private WallValidatorService wallValidatorService;

    static final Integer SQUARE_METERS_WITH_A_LITER_OF_PAINT = 5;

    public Double amountOfLitersNeeded(List<Wall> wallList) {
        Double windowsAndDoorsArea = wallValidatorService.getWindowsAndDoorsAreaByWallList(wallList);
        Double totalArea = wallList.stream().mapToDouble(Wall::getArea).sum() - windowsAndDoorsArea;
        return totalArea / SQUARE_METERS_WITH_A_LITER_OF_PAINT;
    }

    public LinkedHashMap<Double, Integer> paintCansNeeded(List<Wall> wallList) {
        Double amountOfLiters = amountOfLitersNeeded(wallList);
        LinkedHashMap<Double, Integer> cansQuantity = new LinkedHashMap<>();
        cansQuantity.put(18d, 0);
        cansQuantity.put(3.6, 0);
        cansQuantity.put(2.5, 0);
        cansQuantity.put(0.5, 0);
        do {
            if (amountOfLiters >= 18) {
                amountOfLiters -= 18;
                cansQuantity.put(18d, cansQuantity.get(18d) + 1);
            } else if (amountOfLiters >= 3.6) {
                amountOfLiters -= 3.6;
                cansQuantity.put(3.6, cansQuantity.get(3.6) + 1);
            } else if (amountOfLiters >= 2.5) {
                amountOfLiters -= 2.5;
                cansQuantity.put(2.5, cansQuantity.get(2.5) + 1);
            } else {
                amountOfLiters -= 0.5;
                cansQuantity.put(0.5, cansQuantity.get(0.5) + 1);
            }
        } while (amountOfLiters > 0);
        return cansQuantity;
    }

    public ResponseDTO calculate(RequestDTO requestDTO) {
        Boolean allWallIsValid = Boolean.TRUE;
        List<Wall> wallList = requestDTO.getWall();
        if (wallList.size() == 4) {
            for (Wall wall : wallList) {
                Boolean isValid = wallValidatorService.inputValidator(wall);
                if (!isValid) {
                    allWallIsValid = Boolean.FALSE;
                    break;
                }
            }
        } else {
            allWallIsValid = Boolean.FALSE;
        }

        ResponseDTO responseDTO = new ResponseDTO();
        if (allWallIsValid) {
            responseDTO.setResponseMessage(getResponseMessage(wallList, Boolean.TRUE));
        } else {
            responseDTO.setErrorMessage(getResponseMessage(wallList, Boolean.FALSE));
        }
        return responseDTO;
    }

    private String getResponseMessage(List<Wall> wallList, Boolean allWallIsValid) {
        if (allWallIsValid) {
            LinkedHashMap<Double, Integer> paintCansNeeded = this.paintCansNeeded(wallList);
            StringBuilder responseMessage = new StringBuilder();
            Integer eighteenLiter = paintCansNeeded.get(18d);
            Integer threePointSixLiters = paintCansNeeded.get(3.6);
            Integer twoPointFiveLiters = paintCansNeeded.get(2.5);
            Integer halfLiter = paintCansNeeded.get(0.5);
            String canOfPaint;
            for (Map.Entry<Double, Integer> canOfPaintEntry : paintCansNeeded.entrySet()) {
                if (canOfPaintEntry.getKey().equals(18d) && eighteenLiter > 0) {
                    canOfPaint = threePointSixLiters > 1 ? " latas " : " lata ";
                    responseMessage.append("Você precisará de " + eighteenLiter + canOfPaint + "de 18L");
                } else if (canOfPaintEntry.getKey().equals(3.6) && threePointSixLiters > 0) {
                    canOfPaint = threePointSixLiters > 1 ? " latas " : " lata ";
                    if (responseMessage.toString().isEmpty()) {
                        responseMessage.append("Você precisará de " + threePointSixLiters + canOfPaint + "de 3,6L");
                    } else {
                        responseMessage.append(" + " + threePointSixLiters + canOfPaint + "de 3,6L");
                    }
                } else if (canOfPaintEntry.getKey().equals(2.5) && twoPointFiveLiters > 0) {
                    canOfPaint = twoPointFiveLiters > 1 ? " latas " : " lata ";
                    if (responseMessage.toString().isEmpty()) {
                        responseMessage.append("Você precisará de " + twoPointFiveLiters + canOfPaint + "de 2,5L");
                    } else {
                        responseMessage.append(" + " + twoPointFiveLiters + canOfPaint + "de 2,5L");
                    }
                } else if (canOfPaintEntry.getKey().equals(0.5) && halfLiter > 0) {
                    canOfPaint = halfLiter > 1 ? " latas " : " lata ";
                    if (responseMessage.toString().isEmpty()) {
                        responseMessage.append("Você precisará de " + halfLiter + canOfPaint + "de 0,5L");
                    } else {
                        responseMessage.append(" + " + halfLiter + canOfPaint + "de 0,5L");
                    }
                }
            }
            return responseMessage.toString();
        } else {
            return "A entrada está inválida. Fique atento(a) para as seguintes regras:\n" +
                    "1 - Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados, mas podem possuir alturas e larguras diferentes;\n" +
                    "2 - O total de área das portas e janelas deve ser no máximo 50% da área de parede;\n" +
                    "3 - A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta;\n" +
                    "4 - Cada janela possui as medidas: 2,00 x 1,20 mtos;\n" +
                    "5 - Cada porta possui as medidas: 0,80 x 1,90;\n" +
                    "6 - É necessário que a lista de paredes tenha 4 paredes com informações válidas;\n";
        }
    }
}