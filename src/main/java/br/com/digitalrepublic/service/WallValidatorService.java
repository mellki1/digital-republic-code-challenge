package br.com.digitalrepublic.service;

import br.com.digitalrepublic.model.Wall;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WallValidatorService {

    static final double WINDOW_WIDTH = 1.20;
    static final double DOOR_WIDTH = 1.90;
    static final double WINDOW_HEIGHT = 2.00;
    static final double DOOR_HEIGHT = 0.80;
    static final double WINDOW_AREA = WINDOW_HEIGHT * WINDOW_WIDTH;
    static final double DOOR_AREA = DOOR_HEIGHT * DOOR_WIDTH;

    public Boolean inputValidator(Wall wall) {
        Boolean wallSizeIsValid = wallSizeValidator(wall.getArea());
        Boolean percentageWithWindowsAndDoorsIsValid = wallSizeIsValid ? windowsAndDoorsQuantityValidator(wall) : Boolean.FALSE;
        return Boolean.TRUE.equals(wallSizeIsValid && percentageWithWindowsAndDoorsIsValid);
    }

    public Boolean wallSizeValidator(Double area) {
        return Boolean.TRUE.equals(area >= 1 && area <= 15);
    }

    public Boolean windowsAndDoorsQuantityValidator(Wall wall) {
        Boolean hasWindow = Boolean.TRUE.equals(wall.getWindowsQuantity() > 0);
        Boolean hasDoor = Boolean.TRUE.equals(wall.getDoorsQuantity() > 0);
        if (hasWindow || hasDoor) {
            if (hasDoor && !((wall.getWidth() - 0.30) >= DOOR_WIDTH))
                return Boolean.FALSE;
            Double wallArea = wall.getArea();
            Double windowsArea = wall.getWindowsQuantity() * WINDOW_AREA;
            Double doorsArea = wall.getDoorsQuantity() * DOOR_AREA;
            Double windowsAndDoorsArea = windowsArea + doorsArea;
            Double percentageWithWindowsAndDoors = (100 * windowsAndDoorsArea) / wallArea;
            return Boolean.TRUE.equals(percentageWithWindowsAndDoors < 50);
        } else {
            return Boolean.TRUE;
        }
    }

    public Double getWindowsAndDoorsAreaByWallList(List<Wall> wallList) {
        Double total = 0d;
        for (Wall wall : wallList) {
            Double windowsArea = wall.getWindowsQuantity() * WINDOW_AREA;
            Double doorsArea = wall.getDoorsQuantity() * DOOR_AREA;
            total += windowsArea + doorsArea;
        }
        return total;
    }
}
