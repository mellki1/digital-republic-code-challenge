package br.com.digitalrepublic.controller;

import br.com.digitalrepublic.dto.RequestDTO;
import br.com.digitalrepublic.dto.ResponseDTO;
import br.com.digitalrepublic.service.PaintRoomService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("paint-room")
public class PaintRoomController {

    @Autowired
    private PaintRoomService paintRoomService;

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna uma String informando quantas latas serão necessárias para pintar a parede"),
            @ApiResponse(code = 400, message = "Retorna uma menssagem de erro informando as regras necessárias para obter um resultado correto.")
    })
    @PostMapping()
    public ResponseEntity<?> paintRoom(@RequestBody RequestDTO requestDTO) {
        ResponseDTO responseDTO = paintRoomService.calculate(requestDTO);
        if (responseDTO.getErrorMessage() == null) {
            return ResponseEntity.ok(responseDTO.getResponseMessage());
        } else {
            return ResponseEntity.badRequest().body(responseDTO.getErrorMessage());
        }
    }
}
